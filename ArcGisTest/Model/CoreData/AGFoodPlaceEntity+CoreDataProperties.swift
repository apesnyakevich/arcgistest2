//
//  AGFoodPlaceEntity+CoreDataProperties.swift
//  ArcGisTest
//
//  Created by Andrew on 6/21/19.
//  Copyright © 2019 Andrew. All rights reserved.
//
//

import Foundation
import CoreData


extension AGFoodPlaceEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AGFoodPlaceEntity> {
        return NSFetchRequest<AGFoodPlaceEntity>(entityName: "AGFoodPlaceEntity")
    }

    @NSManaged public var name: String?
    @NSManaged public var fullAddress: String?
    @NSManaged public var distance: Float
    @NSManaged public var latitude: Float
    @NSManaged public var longitude: Float
    @NSManaged public var phone: String?
    @NSManaged public var url: String?

}
