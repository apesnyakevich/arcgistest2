//
//  AGFoodPlaceEntity+Unboxable.swift
//  ArcGisTest
//
//  Created by Andrew on 21.09.2018.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import Unbox

extension AGFoodPlaceEntity {
    
    func performUnboxing(_ unboxer: Unboxer) throws {
        self.name = try unboxer.unbox(key: AGPlaceAttributes.name)
        self.fullAddress = try unboxer.unbox(key: AGPlaceAttributes.fullAddress)
        self.phone = try unboxer.unbox(key: AGPlaceAttributes.phone)
        self.url = try unboxer.unbox(key: AGPlaceAttributes.url)
        
        do {
            let lat: Float = try unboxer.unbox(key: AGPlaceAttributes.latitude)
            self.latitude = lat
            
            let long: Float = try unboxer.unbox(key: AGPlaceAttributes.longitude)
            self.longitude = long
            
            let dist: Float = try unboxer.unbox(key: AGPlaceAttributes.distance)
            self.distance = dist
        } catch {
            print("driver rating error occurred: \(error)")
        }
    }
}
