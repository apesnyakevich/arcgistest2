//
//  AGPlaceListModel.swift
//  ArcGisTest
//
//  Created by Andrew on 6/21/19.
//  Copyright © 2019 Andrew. All rights reserved.
//

struct AGPlaceListModel {
    var places: [AGFoodPlaceEntity] = []
}

struct AGPlaceListCellModel {
    var name: String?
    var address: String?
}
