//
//  AGPlaceListTableViewCell.swift
//  ArcGisTest
//
//  Created by Andrew on 11.09.2018.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

class AGPlaceListTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    
    func setup(with model: AGPlaceListCellModel?) {
        nameLabel.text = model?.name
        addressLabel.text = model?.address
    }
}
