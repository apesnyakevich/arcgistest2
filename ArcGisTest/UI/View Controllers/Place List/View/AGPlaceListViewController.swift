//
//  AGPlaceListViewController.swift
//  ArcGisTest
//
//  Created by Andrew on 6/21/19.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import GoogleMaps

protocol AGPlaceListViewControllerProtocol: AnyObject {
    func needUpdatePlaces()
    func needCenterMapAt(_ coordinate: CLLocationCoordinate2D)
}

class AGPlaceListViewController: UIViewController {

    //MARK: -
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var placesTableView: UITableView!
    
    var viewModel: AGPlaceListViewModelProtocol!
    
    let mapZoomLevel: Float = 14.0
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Food Places"
        
        viewModel.setupLocalPlaces()
        viewModel.setupLocationSettings()
        
        setupMapView()
        setupTableView()
    }
    
    //MARK: - Private
    
    private func setupTableView() {
        placesTableView.delegate = self
        placesTableView.dataSource = self
    }
    
    private func setupMapView() {
        // Setup a map.
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.settings.zoomGestures = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = false
        mapView.delegate = self
        
        if let currentLocation = viewModel.currentLocationCoordinate, let _ = viewModel.lastUpdateLocation {
            let camera = GMSCameraPosition.camera(withLatitude: currentLocation.latitude,
                                                  longitude: currentLocation.longitude,
                                                  zoom: mapZoomLevel)
            mapView.animate(to: camera)
            viewModel.reloadPlacesAt(currentLocation)
        }
    }
}

extension AGPlaceListViewController: AGPlaceListViewControllerProtocol {
    
    func needUpdatePlaces() {
        placesTableView.reloadData()
        reloadPlaceMarkers()
    }
    
    func needCenterMapAt(_ coordinate: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude,
                                              longitude: coordinate.longitude,
                                              zoom: mapZoomLevel)
        mapView.animate(to: camera)
    }
}

