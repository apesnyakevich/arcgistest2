//
//  AGPlaceListViewController+TableView.swift
//  ArcGisTest
//
//  Created by Andrew on 11.09.2018.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

extension AGPlaceListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard (viewModel.places.count > indexPath.row) else { return }
        
        let place = viewModel.places[indexPath.row]
        AGNavigationService.showPlaceDetails(place: place)
    }
}

extension AGPlaceListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.placeCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = String(describing: AGPlaceListTableViewCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier:identifier, for: indexPath) 
        
        let cellModel = viewModel.placeCellModelAt(indexPath)
        (cell as! AGPlaceListTableViewCell).setup(with:cellModel)

        return cell
    }
}
