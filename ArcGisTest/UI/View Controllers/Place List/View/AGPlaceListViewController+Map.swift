//
//  AGPlaceListViewController+Map.swift
//  ArcGisTest
//
//  Created by Andrew on 9/17/18.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import GoogleMaps

protocol AGPlaceListViewControllerMapProtocol {
    func clearMap()
    func reloadPlaceMarkers()
}

extension AGPlaceListViewController: GMSMapViewDelegate {
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if let location = viewModel.currentLocationCoordinate {
            let camera = GMSCameraPosition.camera(withLatitude: location.latitude,
                                                  longitude: location.longitude,
                                                  zoom: mapZoomLevel)
            mapView.animate(to: camera)
        }
        return true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        debugPrint(position.target)
        
        if let _ = viewModel.lastUpdateLocation {
            viewModel.reloadPlacesAt(position.target)
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.selectedMarker = marker
        return true
    }
}

extension AGPlaceListViewController: AGPlaceListViewControllerMapProtocol {
    
    func clearMap() {
        mapView.clear()
    }
    
    func reloadPlaceMarkers() {
        clearMap()
        
        for place in viewModel.places {
            let location = CLLocationCoordinate2DMake(Double(place.latitude), Double(place.longitude))
            
            let placeMarker = GMSMarker()
            placeMarker.position = location
            placeMarker.title = place.name
            placeMarker.snippet = place.fullAddress
            placeMarker.icon = UIImage(named: "icon_marker")
            placeMarker.map = mapView
        }
    }
}
