//
//  AGPlaceListViewModel.swift
//  ArcGisTest
//
//  Created by Andrew on 6/21/19.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import CoreLocation

protocol AGPlaceListViewModelProtocol {
    func setupLocalPlaces()
    func setupLocationSettings()
    func reloadPlacesAt(_ location: CLLocationCoordinate2D)
    
    func isNeedUpdateLocation(_ location: CLLocationCoordinate2D) -> Bool
    var lastUpdateLocation: CLLocationCoordinate2D? { get }
    
    var places: [AGFoodPlaceEntity] { get }
    var placeCount: Int { get }
    func placeCellModelAt(_ indexPath: IndexPath) -> AGPlaceListCellModel?
    
    var currentLocationCoordinate: CLLocationCoordinate2D? { get }
}

class AGPlaceListViewModel: NSObject {
    
    unowned var view: AGPlaceListViewControllerProtocol
    var model: AGPlaceListModel
    
    var dataProvider: AGDataProviderProtocol?
    var placesLocatorService: AGPlacesLocatorServiceProtocol?
    
    let locationManager = CLLocationManager()
    
    init(view: AGPlaceListViewControllerProtocol, model: AGPlaceListModel, dataProvider: AGDataProviderProtocol?, placesLocatorService: AGPlacesLocatorServiceProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
        self.placesLocatorService = placesLocatorService
    }
}

extension AGPlaceListViewModel: AGPlaceListViewModelProtocol {
    
    func setupLocalPlaces() {
        self.model.places.removeAll()
        if let places = self.dataProvider?.allFoodPlaces() {
            self.model.places.append(contentsOf: places)
        }
    }
    
    func reloadPlacesAt(_ location: CLLocationCoordinate2D) {
        
        if (!isNeedUpdateLocation(location)) {
            DispatchQueue.main.async {
                self.view.needUpdatePlaces()
            }
            return
        }
        
        placesLocatorService?.findFoodPlacesAt(location, { [weak self] (places) in
            guard let self = self else { return }
            guard let places = places, places.count > 0 else { return }
            
            AGAppUtils.setLastUpdateLocationLatitude(location.latitude)
            AGAppUtils.setLastUpdateLocationLongitude(location.longitude)
            
            self.model.places.removeAll()
            self.dataProvider?.deleteAllFoodPlaces()
            
            self.dataProvider?.insertFoodPlaces(places: places, context: nil, completion: { (completed) in
                if (completed) {
                    if let newPlaces = self.dataProvider?.allFoodPlaces() {
                        self.model.places.append(contentsOf: newPlaces)
                    }
                    DispatchQueue.main.async {
                        self.view.needUpdatePlaces()
                    }
                }
            })
        })
    }
    
    func isNeedUpdateLocation(_ location: CLLocationCoordinate2D) -> Bool {
        var isNeedUpdate = true
        
        if let lastLatitude = AGAppUtils.lastUpdateLocationLatitude(), let lastLongitude = AGAppUtils.lastUpdateLocationLongitude() {
            let lastLocation = CLLocation(latitude: lastLatitude, longitude: lastLongitude)
            let currentLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
            let distanceInMeters = currentLocation.distance(from: lastLocation)
            
            isNeedUpdate = (distanceInMeters > 100)
        }
        
        return isNeedUpdate
    }
    
    var lastUpdateLocation: CLLocationCoordinate2D? {
        get {
            var location: CLLocationCoordinate2D? = nil
            if let lastLatitude = AGAppUtils.lastUpdateLocationLatitude(), let lastLongitude = AGAppUtils.lastUpdateLocationLongitude() {
                location = CLLocationCoordinate2DMake(lastLatitude, lastLongitude)
            }
            return location
        }
    }
    
    var places: [AGFoodPlaceEntity] {
        get {
            return model.places
        }
    }
    
    var placeCount: Int {
        get {
           return model.places.count
        }
    }
    
    func placeCellModelAt(_ indexPath: IndexPath) -> AGPlaceListCellModel? {
        guard (model.places.count >  indexPath.row) else { return nil }
        
        let place = model.places[indexPath.row]
        var cellModel = AGPlaceListCellModel()
        cellModel.address = place.fullAddress
        cellModel.name = place.name
        
        return cellModel
    }
    
    var currentLocationCoordinate: CLLocationCoordinate2D? {
        get {
            var coordinates: CLLocationCoordinate2D?
            if let currentLocation = locationManager.location {
                coordinates = currentLocation.coordinate
            }
            return coordinates
        }
    }
}
