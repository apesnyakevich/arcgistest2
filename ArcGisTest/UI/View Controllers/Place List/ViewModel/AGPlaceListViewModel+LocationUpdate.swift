//
//  AGPlaceListViewModel.swift
//  ArcGisTest
//
//  Created by Andrew on 9/10/18.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import MapKit
import Unbox
import GoogleMaps

extension AGPlaceListViewModel {
    
    func setupLocationSettings() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                debugPrint("location update denied")
                break
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.startUpdatingLocation()
            }
        }
    }
}

extension AGPlaceListViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == .authorizedWhenInUse || status == .authorizedAlways) {
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = manager.location, lastUpdateLocation == nil {
            reloadPlacesAt(currentLocation.coordinate)
            view.needCenterMapAt(currentLocation.coordinate)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        debugPrint("location update error")
    }
}
