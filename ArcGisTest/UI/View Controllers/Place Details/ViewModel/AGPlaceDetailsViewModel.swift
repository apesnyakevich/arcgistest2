//
//  AGPlaceDetailsViewModel.swift
//  ArcGisTest
//
//  Created by Andrew on 6/21/19.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

protocol AGPlaceDetailsViewModelProtocol {
    var placeName: String? { get }
    var placeDescription: String? { get }
}

class AGPlaceDetailsViewModel: NSObject {
    
    unowned var view: AGPlaceDetailsViewControllerProtocol
    var model: AGPlaceDetailsModel
    
    var place: AGFoodPlaceEntity?
    
    init(view: AGPlaceDetailsViewControllerProtocol, model: AGPlaceDetailsModel) {
        self.view = view
        self.model = model
    }
}

extension AGPlaceDetailsViewModel: AGPlaceDetailsViewModelProtocol {
    
    var placeName: String? {
        get  {
            return place?.name
        }
    }
    
    var placeDescription: String? {
        get {
            var description = "\n"
            if let address = place?.fullAddress, !address.isEmpty {
                description.append("Address:\n" + address + "\n\n")
            }
            if let phone = place?.phone, !phone.isEmpty {
                description.append("Phone:\n" + phone + "\n\n")
            }
            if let url = place?.url, !url.isEmpty {
                description.append("URL:\n" + url + "\n\n")
            }
            if let distance = place?.distance, distance > 0 {
                description.append("Distance:\n" + String(format:"%.1f", (distance/1000)) + "km")
            }
            
            return description
        }
    }
}
