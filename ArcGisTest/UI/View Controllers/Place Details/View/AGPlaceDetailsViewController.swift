//
//  AGPlaceDetailsViewController.swift
//  ArcGisTest
//
//  Created by Andrew on 6/21/19.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit

protocol AGPlaceDetailsViewControllerProtocol: AnyObject {
    
}

class AGPlaceDetailsViewController: UIViewController {
    
    @IBOutlet weak var detailsTextView: UITextView!
    
    var viewModel: AGPlaceDetailsViewModelProtocol!
    
    //MARK: - View lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = viewModel.placeName
        detailsTextView.text = viewModel.placeDescription
    }
}

extension AGPlaceDetailsViewController: AGPlaceDetailsViewControllerProtocol {
    
}

