//
//  AGNavigationService.swift
//  ArcGistest
//
//  Created by Andrew on 8/27/18.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import SwinjectStoryboard
import Swinject

class AGNavigationService: NSObject {
    
    private static var swinjectContainer: Container {
        return SwinjectStoryboard.defaultContainer
    }
    
    //MARK: -
    
    static func showPlaceList() {
        let placesViewController = AGScreenAssembler.placeListScreen(container: swinjectContainer)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainNC = appDelegate.rootController
        mainNC.show(placesViewController, sender: nil)
    }
    
    static func showPlaceDetails(place: AGFoodPlaceEntity?) {
        let placeViewController = AGScreenAssembler.placeDetailsScreen(container: swinjectContainer, place: place)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainNC = appDelegate.rootController
        mainNC.show(placeViewController, sender: nil)
    }
}
