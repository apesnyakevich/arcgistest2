//
//  AGDataProvider+FoodPlace.swift
//  ArcGisTest
//
//  Created by Andrew on 21.09.2018.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import MagicalRecord
import Unbox

extension AGDataProvider {
    
    //MARK: - SELECT
    
    func allFoodPlaces() -> [AGFoodPlaceEntity] {
        let places = AGFoodPlaceEntity.mr_findAllSorted(by: "name", ascending: false)
        return places as! [AGFoodPlaceEntity]
    }
    
    
    //MARK: - INSERT
    
    func insertFoodPlaces(places: [Any], context: NSManagedObjectContext?, completion: @escaping (_ completed: Bool)->Void) {
        let moc = (context != nil) ? context : mainContext()
        
        for placeInfo in places {
            let place = AGFoodPlaceEntity.mr_createEntity(in: mainContext())
            do {
                if (placeInfo is UnboxableDictionary) {
                    try Unboxer.performCustomUnboxing(dictionary: placeInfo as! UnboxableDictionary, closure: { unboxer in
                        try place?.performUnboxing(unboxer)
                    })
                }
            } catch let error as NSError {
                print(error)
            }
        }
        
        moc!.mr_saveToPersistentStore { (success, error) in
            completion(success)
        }
    }
    
    //MARK: - DELETE
    
    func deleteAllFoodPlaces() {
        AGFoodPlaceEntity.mr_deleteAll(matching: NSPredicate(value: true))
        mainContext().mr_saveToPersistentStoreAndWait()
    }
}
