//
//  AGDataProvider.swift
//  ArcGisTest
//
//  Created by Andrew on 26/03/2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import UIKit
import MagicalRecord

class AGDataProvider: NSObject, AGDataProviderProtocol {
    
    //MARK: - Context
    
    func mainContext() -> NSManagedObjectContext {
        return NSManagedObjectContext.mr_default()
    }
    
    func childContextWithParent(_ parent: NSManagedObjectContext?) -> NSManagedObjectContext {
        let childContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        
        if (parent != nil) {
            childContext.parent = parent
        } else {
            childContext.parent = mainContext()
        }
        return childContext
    }
    
    func saveMainContextWithCompletion(_ completion: @escaping (Bool, Error?) -> Swift.Void?) {
        mainContext().mr_saveToPersistentStore(completion: { (success, error) in
            completion(success, error)
        })
    }
    
    func resetMainContext() {
        mainContext().reset()
    }
    
    func saveContext(_ context: NSManagedObjectContext?, completion:((Bool, Error?) -> Swift.Void)?) {
        let moc = (context != nil) ? context : mainContext()
        moc?.mr_saveToPersistentStore(completion: { (success, error) in
            completion?(success, error)
        })
    }
    
    func resetContext(_ context: NSManagedObjectContext) {
        context.reset()
    }
}
