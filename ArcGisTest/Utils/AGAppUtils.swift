//
//  AGAppUtils.swift
//  ArcGisTest
//
//  Created by Andrew on 9/4/18.
//  Copyright © 2019 Andrew. All rights reserved.
//

import UIKit
import MapKit

class AGAppUtils: NSObject {
    //MARK: - User Defaults
    
    static func lastUpdateLocationLatitude() -> CLLocationDegrees? {
        let defaults:UserDefaults = UserDefaults.standard
        let latitude = defaults.object(forKey: "LastUpdateLocationLatitude") as! CLLocationDegrees?
        return latitude
    }
    
    static func setLastUpdateLocationLatitude(_ latitude: CLLocationDegrees?) {
        let defaults:UserDefaults = UserDefaults.standard
        defaults.set(latitude, forKey: "LastUpdateLocationLatitude")
    }
    
    static func lastUpdateLocationLongitude() -> CLLocationDegrees? {
        let defaults:UserDefaults = UserDefaults.standard
        let longitude = defaults.object(forKey: "LastUpdateLocationLongitude") as! CLLocationDegrees?
        return longitude
    }
    
    static func setLastUpdateLocationLongitude(_ longitude: CLLocationDegrees?) {
        let defaults:UserDefaults = UserDefaults.standard
        defaults.set(longitude, forKey: "LastUpdateLocationLongitude")
    }
}
